﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class WorldLoader : MonoBehaviour
{
    
    public string VRC_USERNAME = "snail";
    public string VRC_PASSWORD = "";
    public string WorldId = "wrld_dc124ed6-acec-4d55-9866-54ab66af172d";
    public bool StartLoading = false;

    [System.Serializable]
    class WorldResponse
    {
        public string assetUrl;
    }
    void Update(){
        if(StartLoading) {
            StartLoading = false;
            StartCoroutine(Load());
        }
    }

    IEnumerator Load()
    {
        string uri = $"https://api.vrchat.cloud/api/1/worlds/{WorldId}/metadata?apiKey=JlE5Jldo5Jibnk5O5hTx6XVqsJu4WJ26";
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            
            string auth = $"{VRC_USERNAME}:{VRC_PASSWORD}";
            auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
//            webRequest.SetRequestHeader("AUTHORIZATION", "Basic " + auth);

            Debug.Log("uri " + uri);
            yield return webRequest.SendWebRequest();
            Debug.Log("Response");
            Debug.Log(webRequest.downloadHandler.text);

            var obj = JsonUtility.FromJson<WorldResponse>(webRequest.downloadHandler.text);
            uri = obj.assetUrl;
        }

        // using (UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(uri))
        // {
        //     Debug.Log(uri);
        //     yield return www.SendWebRequest();
        //     AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
        //     foreach (string scenePath in bundle.GetAllScenePaths())
        //     {
        //         string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePath);

        //         if (SceneManager.sceneCount > 1)
        //             yield return SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(1));

        //         yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
                
        //         SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
        //     }
        //     bundle.Unload(false);
        // }
    }
}
