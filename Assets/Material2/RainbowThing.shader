// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "RainbowThing"
{
	Properties
	{
		_MainTex("_MainTex", 2D) = "white" {}
		[HDR]_EmissionColor("_EmissionColor", Color) = (1,1,1,0)
		_EmissionMap("_EmissionMap", 2D) = "white" {}
		_Vector3("Vector 3", Vector) = (0,0,0,0)
		_Apply("Apply", Float) = 1
		_Vector4("Vector 4", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf StandardCustomLighting keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			float3 viewDir;
			float2 uv2_texcoord2;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float4 _Vector4;
		uniform float _Apply;
		uniform sampler2D _EmissionMap;
		uniform float4 _EmissionMap_ST;
		uniform float4 _EmissionColor;
		uniform float2 _Vector3;


		half3 SH9(  )
		{
			return ShadeSH9(fixed4(0,0,0,1));
		}


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, saturate( p - K.xxx ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			half3 localSH98_g11 = SH9();
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 temp_output_12_0 = ( float4( ( localSH98_g11 + (( ase_lightColor.rgb * ( float3(1,1,1) * 1.0 ) * ase_lightAtten )).xyz ) , 0.0 ) * tex2D( _MainTex, uv_MainTex ) );
			float3 ase_worldNormal = i.worldNormal;
			Unity_GlossyEnvironmentData g94 = UnityGlossyEnvironmentSetup( 0.5, data.worldViewDir, ase_worldNormal, float3(0,0,0));
			float3 indirectSpecular94 = UnityGI_IndirectSpecular( data, 1.0, ase_worldNormal, g94 );
			float luminance96 = Luminance(indirectSpecular94);
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float dotResult85 = dot( i.viewDir , ase_vertexNormal );
			float mulTime88 = _Time.y * _Vector4.w;
			float hue63 = (luminance96*_Vector4.y + (dotResult85*_Vector4.z + mulTime88));
			float3 hsvTorgb28 = RGBToHSV( temp_output_12_0.rgb );
			float3 hsvTorgb29 = HSVToRGB( float3(( frac( hue63 ) + hsvTorgb28.x ),hsvTorgb28.y,hsvTorgb28.z) );
			float2 _Vector0 = float2(1,1);
			float ifLocalVar22 = 0;
			if( i.uv2_texcoord2.y <= _Vector0.x )
				ifLocalVar22 = _Vector0.x;
			else
				ifLocalVar22 = _Vector0.y;
			float Apply41 = ( ifLocalVar22 * _Apply );
			float4 lerpResult25 = lerp( temp_output_12_0 , float4( hsvTorgb29 , 0.0 ) , Apply41);
			float2 uv_EmissionMap = i.uv_texcoord * _EmissionMap_ST.xy + _EmissionMap_ST.zw;
			float4 temp_output_55_0 = ( tex2D( _EmissionMap, uv_EmissionMap ) * _EmissionColor );
			float3 hsvTorgb56 = RGBToHSV( temp_output_55_0.rgb );
			float3 hsvTorgb57 = HSVToRGB( float3(( hue63 + hsvTorgb56.x ),hsvTorgb56.y,hsvTorgb56.z) );
			float4 lerpResult59 = lerp( temp_output_55_0 , float4( hsvTorgb57 , 0.0 ) , Apply41);
			float dotResult69 = dot( ase_worldNormal , i.viewDir );
			float smoothstepResult70 = smoothstep( _Vector3.x , _Vector3.y , dotResult69);
			float4 temp_output_66_0 = ( lerpResult25 + ( lerpResult59 * smoothstepResult70 ) );
			c.rgb = temp_output_66_0.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18800
940;81;970;911;-666.9344;1309.234;1.016441;False;False
Node;AmplifyShaderEditor.NormalVertexDataNode;84;762.8414,-994.0709;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;83;773.8414,-1139.071;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector4Node;91;771.2625,-852.4916;Inherit;False;Property;_Vector4;Vector 4;10;0;Create;True;0;0;0;False;0;False;0,0,0,0;32.86,3,0.1,0.1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.IndirectSpecularLight;94;711.3105,-1259.689;Inherit;False;Tangent;3;0;FLOAT3;0,0,1;False;1;FLOAT;0.5;False;2;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleTimeNode;88;1034.7,-762.946;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;85;950.8414,-1070.071;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;86;1350.499,-1074.071;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LuminanceNode;96;955.4688,-1254.209;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;2;-481.7488,316.4556;Inherit;True;Property;_EmissionMap;_EmissionMap;5;0;Create;True;0;0;0;False;0;False;None;ba7dd7cd42c8ecc4e9a9c0e1886d02a0;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.TextureCoordinatesNode;21;-403.1978,-1187.897;Inherit;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;7;-189.9061,496.8384;Inherit;False;Property;_EmissionColor;_EmissionColor;4;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,0;0.5019608,0.5019608,0.5019608,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;1;-481.7604,-48.43803;Inherit;True;Property;_MainTex;_MainTex;0;0;Create;True;0;0;0;False;0;False;None;0cf3a43333e732f4f8617b011ae13495;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SamplerNode;5;-277.0027,317.0087;Inherit;True;Property;_TextureSample1;Texture Sample 1;4;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;24;-332.4991,-1075.703;Inherit;False;Constant;_Vector0;Vector 0;6;0;Create;True;0;0;0;False;0;False;1,1;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.ScaleAndOffsetNode;92;1371.354,-1256.407;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;63;1587.127,-1257.863;Inherit;False;hue;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;79;-142.9507,-899.1331;Inherit;False;Property;_Apply;Apply;9;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;4;-277.5792,-48.12607;Inherit;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;55;38.43523,316.305;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;82;-264.046,-238.1813;Inherit;False;FlatLighting;1;;11;a37efa9dd8becc748a82bbf7d3609977;1,27,1;4;30;FLOAT;1;False;25;FLOAT3;0,0,0;False;22;FLOAT;0;False;20;SAMPLER2D;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ConditionalIfNode;22;-154.1479,-1085.023;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RGBToHSVNode;56;172.5372,377.7188;Inherit;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;65;193.5352,306.5274;Inherit;False;63;hue;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;64;28.0118,-262.9405;Inherit;False;63;hue;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;78;33.04926,-915.1331;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;-15.83822,-137.1053;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RGBToHSVNode;28;120.4051,-57.1457;Inherit;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;67;311.0424,764.0411;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;41;166.3713,-913.7024;Inherit;False;Apply;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;77;186.6085,-260.8871;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;68;303.0027,629.8907;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;60;367.0096,356.2384;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;71;486.3824,813.1712;Inherit;False;Property;_Vector3;Vector 3;8;0;Create;True;0;0;0;False;0;False;0,0;1,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.HSVToRGBNode;57;475.437,384.2187;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;69;487.0023,688.8907;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;58;513.6008,516.7274;Inherit;False;41;Apply;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;61;334.0962,-80.96151;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;44;538.9266,212.1462;Inherit;False;41;Apply;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;29;459.5391,-46.65069;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SmoothstepOpNode;70;703.4989,698.5336;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;59;718.5789,326.4038;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;25;730.4052,-120.5066;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;72;982.1749,434.8642;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;47;-1636.601,-716.7084;Inherit;False;42;Phase;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;33;-686.4114,-571.4345;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FloorOpNode;75;-1142.602,-623.1792;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;76;-1034.602,-619.1792;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;42;-144.5446,-1178.841;Inherit;False;Phase;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;40;-569.3821,-457.5942;Inherit;False;Constant;_Vector2;Vector 2;10;0;Create;True;0;0;0;False;0;False;0,0.1618;0,0.1618;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;107;1135.84,-296.4147;Inherit;False;Constant;_Float5;Float 5;12;0;Create;True;0;0;0;False;0;False;0.01;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;103;1083.389,-222.9268;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;37;-864.5115,-506.4343;Inherit;False;Constant;_Vector1;Vector 1;9;0;Create;True;0;0;0;False;0;False;0,0.5;0,0.5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;100;1429.968,-158.8385;Inherit;False;Property;_Float3;Float 3;12;0;Create;True;0;0;0;False;0;False;0;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;46.23322,525.2523;Inherit;False;Property;_Metallic;Metallic;7;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;108;1209.84,-149.4146;Inherit;False;Property;_Vector5;Vector 5;13;0;Create;True;0;0;0;False;0;False;0,0,1;0,0,0.01;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;73;-1267.602,-619.1792;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;101;1614.396,-111.6518;Inherit;False;Property;_Float4;Float 4;11;0;Create;True;0;0;0;False;0;False;0;-0.88;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;35;-793.0117,-571.4343;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;106;1319.84,-439.4147;Inherit;False;Stepper;-1;;12;c06626409ec7c9745a6618145c7ec8d0;0;2;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;4;FLOAT4;4;FLOAT4;5;FLOAT4;10;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;62;-376.0699,-556.6642;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;42.23322,604.2523;Inherit;False;Property;_Smoothness;Smoothness;14;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;99;1117.79,-430.9619;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;32;-1773.051,-576.098;Inherit;False;Constant;_Float0;Float 0;8;0;Create;True;0;0;0;False;0;False;-1;-0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;52;-1463.141,-674.4468;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;98;1574.157,-260.4283;Inherit;False;Simplex3D;True;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;31;-1620.95,-576.098;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;74;-1408.527,-563.8064;Inherit;False;Constant;_Float2;Float 2;13;0;Create;True;0;0;0;False;0;False;5;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RelayNode;50;-920.3669,-620.0499;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;6;-278.205,135.0012;Inherit;True;Property;_TextureSample2;Texture Sample 2;5;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;109;1339.84,-236.4146;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FloorOpNode;34;-795.6115,-635.1342;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;102;1475.388,-255.127;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TexturePropertyNode;3;-482.3703,134.7704;Inherit;True;Property;_BumpMap;_BumpMap;6;0;Create;True;0;0;0;False;0;False;None;None;False;bump;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.ClipNode;97;1791.453,-228.055;Inherit;False;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;53;-1635.141,-652.4468;Inherit;False;Constant;_Float1;Float 1;11;0;Create;True;0;0;0;False;0;False;2;0.35;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;36;-530.4115,-628.6343;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;66;1034.179,102.8904;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1434.91,122.0338;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;RainbowThing;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;88;0;91;4
WireConnection;85;0;83;0
WireConnection;85;1;84;0
WireConnection;86;0;85;0
WireConnection;86;1;91;3
WireConnection;86;2;88;0
WireConnection;96;0;94;0
WireConnection;5;0;2;0
WireConnection;92;0;96;0
WireConnection;92;1;91;2
WireConnection;92;2;86;0
WireConnection;63;0;92;0
WireConnection;4;0;1;0
WireConnection;55;0;5;0
WireConnection;55;1;7;0
WireConnection;22;0;21;2
WireConnection;22;1;24;1
WireConnection;22;2;24;2
WireConnection;22;3;24;1
WireConnection;22;4;24;1
WireConnection;56;0;55;0
WireConnection;78;0;22;0
WireConnection;78;1;79;0
WireConnection;12;0;82;0
WireConnection;12;1;4;0
WireConnection;28;0;12;0
WireConnection;41;0;78;0
WireConnection;77;0;64;0
WireConnection;60;0;65;0
WireConnection;60;1;56;1
WireConnection;57;0;60;0
WireConnection;57;1;56;2
WireConnection;57;2;56;3
WireConnection;69;0;68;0
WireConnection;69;1;67;0
WireConnection;61;0;77;0
WireConnection;61;1;28;1
WireConnection;29;0;61;0
WireConnection;29;1;28;2
WireConnection;29;2;28;3
WireConnection;70;0;69;0
WireConnection;70;1;71;1
WireConnection;70;2;71;2
WireConnection;59;0;55;0
WireConnection;59;1;57;0
WireConnection;59;2;58;0
WireConnection;25;0;12;0
WireConnection;25;1;29;0
WireConnection;25;2;44;0
WireConnection;72;0;59;0
WireConnection;72;1;70;0
WireConnection;33;0;35;0
WireConnection;33;1;37;1
WireConnection;33;2;37;2
WireConnection;75;0;73;0
WireConnection;76;0;75;0
WireConnection;76;1;74;0
WireConnection;42;0;21;2
WireConnection;73;0;52;0
WireConnection;73;1;74;0
WireConnection;35;0;50;0
WireConnection;106;1;99;0
WireConnection;106;2;107;0
WireConnection;62;0;36;0
WireConnection;62;1;40;2
WireConnection;52;0;47;0
WireConnection;52;1;53;0
WireConnection;52;2;31;0
WireConnection;98;0;102;0
WireConnection;98;1;100;0
WireConnection;31;0;32;0
WireConnection;50;0;76;0
WireConnection;6;0;3;0
WireConnection;109;0;108;0
WireConnection;109;1;103;0
WireConnection;34;0;50;0
WireConnection;102;0;106;4
WireConnection;102;1;109;0
WireConnection;97;0;66;0
WireConnection;97;1;98;0
WireConnection;97;2;101;0
WireConnection;36;0;34;0
WireConnection;36;1;33;0
WireConnection;66;0;25;0
WireConnection;66;1;72;0
WireConnection;0;13;66;0
ASEEND*/
//CHKSM=816CF62B0ECF24FBB7007529936E749DE29B32B1