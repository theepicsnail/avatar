// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Art"
{
	Properties
	{
		_EeAIjUeU0AAfR_E("EeAIjUeU0AAfR_E", 2D) = "white" {}
		_Vector0("Vector 0", Vector) = (0,1,0,0)
		_Vector1("Vector 1", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#include "Assets/Material2/Art/LabColorspace.cginc"
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float3 _Vector0;
		uniform float3 _Vector1;
		uniform sampler2D _EeAIjUeU0AAfR_E;
		uniform float4 _EeAIjUeU0AAfR_E_ST;


		float3 rgb2xyz6_g9( float3 c )
		{
			    float3 tmp;
			    tmp.x = ( c.r > 0.04045 ) ? pow( ( c.r + 0.055 ) / 1.055, 2.4 ) : c.r / 12.92;
			    tmp.y = ( c.g > 0.04045 ) ? pow( ( c.g + 0.055 ) / 1.055, 2.4 ) : c.g / 12.92,
			    tmp.z = ( c.b > 0.04045 ) ? pow( ( c.b + 0.055 ) / 1.055, 2.4 ) : c.b / 12.92;
			    const float3x3 mat = float3x3(
			        0.4124, 0.3576, 0.1805,
			        0.2126, 0.7152, 0.0722,
			        0.0193, 0.1192, 0.9505 
			    );
			    return 100.0 * mul(tmp, mat);
		}


		float3 xyz2lab9_g9( float3 c )
		{
			  float3 n = c / float3(95.047, 100, 108.883);
			    float3 v;
			    v.x = ( n.x > 0.008856 ) ? pow( n.x, 1.0 / 3.0 ) : ( 7.787 * n.x ) + ( 16.0 / 116.0 );
			    v.y = ( n.y > 0.008856 ) ? pow( n.y, 1.0 / 3.0 ) : ( 7.787 * n.y ) + ( 16.0 / 116.0 );
			    v.z = ( n.z > 0.008856 ) ? pow( n.z, 1.0 / 3.0 ) : ( 7.787 * n.z ) + ( 16.0 / 116.0 );
			    return float3(( 116.0 * v.y ) - 16.0, 500.0 * ( v.x - v.y ), 200.0 * ( v.y - v.z ));
		}


		float3 RotateAroundAxis( float3 center, float3 original, float3 u, float angle )
		{
			original -= center;
			float C = cos( angle );
			float S = sin( angle );
			float t = 1 - C;
			float m00 = t * u.x * u.x + C;
			float m01 = t * u.x * u.y - S * u.z;
			float m02 = t * u.x * u.z + S * u.y;
			float m10 = t * u.x * u.y + S * u.z;
			float m11 = t * u.y * u.y + C;
			float m12 = t * u.y * u.z - S * u.x;
			float m20 = t * u.x * u.z - S * u.y;
			float m21 = t * u.y * u.z + S * u.x;
			float m22 = t * u.z * u.z + C;
			float3x3 finalMatrix = float3x3( m00, m01, m02, m10, m11, m12, m20, m21, m22 );
			return mul( finalMatrix, original ) + center;
		}


		float3 lab2xyz7_g8( float3 c )
		{
			 float fy = ( c.x + 16.0 ) / 116.0;
			    float fx = c.y / 500.0 + fy;
			    float fz = fy - c.z / 200.0;
			    return float3(
			         95.047 * (( fx > 0.206897 ) ? fx * fx * fx : ( fx - 16.0 / 116.0 ) / 7.787),
			        100.000 * (( fy > 0.206897 ) ? fy * fy * fy : ( fy - 16.0 / 116.0 ) / 7.787),
			        108.883 * (( fz > 0.206897 ) ? fz * fz * fz : ( fz - 16.0 / 116.0 ) / 7.787)
			    );
		}


		float3 xyz2rgb10_g8( float3 c )
		{
			const float3x3 mat = float3x3(
			        3.2406, -1.5372, -0.4986,
			        -0.9689, 1.8758, 0.0415,
			        0.0557, -0.2040, 1.0570
				);
			    float3 v = mul(c / 100.0, mat);
			    float3 r;
			    r.x = ( v.r > 0.0031308 ) ? (( 1.055 * pow( v.r, ( 1.0 / 2.4 ))) - 0.055 ) : 12.92 * v.r;
			    r.y = ( v.g > 0.0031308 ) ? (( 1.055 * pow( v.g, ( 1.0 / 2.4 ))) - 0.055 ) : 12.92 * v.g;
			    r.z = ( v.b > 0.0031308 ) ? (( 1.055 * pow( v.b, ( 1.0 / 2.4 ))) - 0.055 ) : 12.92 * v.b;
			    return r;
		}


		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_EeAIjUeU0AAfR_E = i.uv_texcoord * _EeAIjUeU0AAfR_E_ST.xy + _EeAIjUeU0AAfR_E_ST.zw;
			float3 temp_output_13_0_g9 = tex2D( _EeAIjUeU0AAfR_E, uv_EeAIjUeU0AAfR_E ).rgb;
			float3 c6_g9 = temp_output_13_0_g9;
			float3 localrgb2xyz6_g9 = rgb2xyz6_g9( c6_g9 );
			float3 c9_g9 = localrgb2xyz6_g9;
			float3 localxyz2lab9_g9 = xyz2lab9_g9( c9_g9 );
			float3 temp_output_28_0 = localxyz2lab9_g9;
			float3 rotatedValue38 = RotateAroundAxis( _Vector1, temp_output_28_0, _Vector0, _Time.y );
			float3 temp_output_13_0_g8 = rotatedValue38;
			float3 c7_g8 = temp_output_13_0_g8;
			float3 locallab2xyz7_g8 = lab2xyz7_g8( c7_g8 );
			float3 c10_g8 = locallab2xyz7_g8;
			float3 localxyz2rgb10_g8 = xyz2rgb10_g8( c10_g8 );
			o.Emission = localxyz2rgb10_g8;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18800
699;81;1211;911;1410.472;604.3992;1;True;False
Node;AmplifyShaderEditor.SamplerNode;1;-1146.515,-46.11482;Inherit;True;Property;_EeAIjUeU0AAfR_E;EeAIjUeU0AAfR_E;0;0;Create;True;0;0;0;False;0;False;-1;e4ab6dad7ea2a1c48ac2d41b39038ad7;beedd29768605c3408c3e499113c3b68;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;40;-977.4716,-320.3992;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;41;-972.4716,-206.3992;Inherit;False;Property;_Vector1;Vector 1;3;0;Create;True;0;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;28;-857.7758,-40.39133;Inherit;False;Colorspace;-1;;9;17e0a76d91d4b1040945a4bbb3c6d439;2,1,0,8,2;1;13;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector3Node;39;-801.4716,-434.3992;Inherit;False;Property;_Vector0;Vector 0;2;0;Create;True;0;0;0;False;0;False;0,1,0;1,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RotateAboutAxisNode;38;-624.4716,-193.3992;Inherit;False;False;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CosTime;32;-1124.182,228.3068;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinTimeNode;33;-1123.792,367.4598;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;35;-938.4716,157.6008;Inherit;False;Constant;_Float0;Float 0;1;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;34;-730.4716,169.6008;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-762.4716,328.6008;Inherit;False;Property;_Float1;Float 1;1;0;Create;True;0;0;0;False;0;False;100;100;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-589.4716,288.6008;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;10;-452.9171,117.7273;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.FunctionNode;27;-113.4989,8.579796;Inherit;False;Colorspace;-1;;8;17e0a76d91d4b1040945a4bbb3c6d439;2,1,2,8,0;1;13;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;272,-92;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;Art;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;1;Include;;True;cc6d320dd6a68a345816f9748e4b2c12;Custom;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;28;13;1;0
WireConnection;38;0;39;0
WireConnection;38;1;40;0
WireConnection;38;2;41;0
WireConnection;38;3;28;0
WireConnection;34;0;35;0
WireConnection;34;1;32;4
WireConnection;34;2;33;4
WireConnection;36;0;34;0
WireConnection;36;1;37;0
WireConnection;10;0;28;0
WireConnection;10;1;36;0
WireConnection;27;13;38;0
WireConnection;0;2;27;0
ASEEND*/
//CHKSM=B431B0BFD6785076CEC3F4B7C8154178AA538FDA